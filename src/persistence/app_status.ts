import fs from "fs/promises"
import RehashShard from "../background_work/rehash_shard"

export default class AppStatus {

    static async readAppStatus() {
        try {
            var configFile = await fs.readFile("./conf/app_status.json")
            var appStatus = JSON.parse(configFile.toString())
            RehashShard.activeShardHashSchemas = appStatus.activeShardHashSchemas
        }
        catch (err) {
            console.log("Unable to find app status file, assuming initial shard creation.")
        }
    }

    static async writeAppStatus() {
        try {
            var appStatus = {
                activeShardHashSchemas: RehashShard.activeShardHashSchemas
            }
            var appStatusString = JSON.stringify(appStatus)
            await fs.writeFile("./conf/app_status.json", appStatusString)
        }
        catch (e) {

        }
    }
}