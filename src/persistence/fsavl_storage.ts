import fs from "fs/promises"
import fsOrig from "fs"
import FsAvlTracking from "../utility/fsavl_tracking";
import RuntimeConfigs from "./runtime_configs";
import { FileResourceError } from "../calls/file_resource";

export default class FsAvlStorage {
    static currentFolder = [0, 0];

    static async initialize() {
        let emptiestFolder: Array<number> = [0, 0];
        let emptiestFolderCount = 10000000000;
        let topLevel = await fs.readdir(RuntimeConfigs.fsRoot)
        topLevel.forEach(async (i) => {
            let secondLevel = await fs.readdir(`${RuntimeConfigs.fsRoot}/${i}`)
            secondLevel.forEach(async (j) => {
                let filesInFolder: number = 0;
                let files = await fs.readdir(`${RuntimeConfigs.fsRoot}/${i}/${j}`)
                files.forEach((fileName) => {
                    let path: string = `${RuntimeConfigs.fsRoot}/${i}/${j}/${fileName}`
                    FsAvlTracking.insert(fileName, path)
                    filesInFolder++;
                })
                if (filesInFolder < emptiestFolderCount) {
                    emptiestFolderCount = filesInFolder
                    emptiestFolder[0] = parseInt(i);
                    emptiestFolder[1] = parseInt(j);
                }
            })
        })
        this.currentFolder = emptiestFolder
    }

    static async saveFile(name: string, file: any): Promise<void> {
        try {
            let fstream: any = fsOrig.createWriteStream(`${RuntimeConfigs.fsRoot}/${this.currentFolder[0]}/${this.currentFolder[1]}/${name}`)
            await file.pipe(fstream);
        }
        catch (err) {
            console.log(err)
            throw new FileResourceError(JSON.stringify({
                msg: "Failed to upload file: " + name
            }), 500)
        }

        FsAvlTracking.insert(name, `${RuntimeConfigs.fsRoot}/${this.currentFolder[0]}/${this.currentFolder[1]}/${name}`)
        if (this.currentFolder[1] >= 99) {
            if (this.currentFolder[0] >= 99) {
                this.currentFolder = [0, 0]
            }
            else {
                this.currentFolder[1] = 0
                this.currentFolder[0]++
            }
        }
        else {
            this.currentFolder[1]++
        }
        return;
    }

    static async deleteFile(name: string): Promise<boolean> {
        let filePath = FsAvlTracking.find(name)
        if (!filePath) {
            return false;
        }
        else {
            try {
                await fs.unlink(filePath)
            }
            catch (err) {
                console.log(err)
                return false;
            }
        }
        FsAvlTracking.delete(name)
        return true;
    }

    static async retrieveFile(name: string) {

    }
}