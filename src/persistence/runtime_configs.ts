import fs from "fs/promises"

export default class RuntimeConfigs {
    static fsRoot: string = "";
    static accessToken: string = "";
    static shardNumber: number = -1;
    static port: string = ""
    static address: string = ""
    static coordinatorAddress: string = ""
    static coordinatorPort: string = ""

    static async readConfigs() {
        try {
            var configFile = await fs.readFile("./conf/app_config.json")
        }
        catch (err) {
            throw new Error("Could not read config file ./conf/app_config.json")

        }

        var configFileJson = JSON.parse(configFile.toString())
        if (!configFileJson.hasOwnProperty("fsRoot")) {
            throw new Error("Config file missing fsRoot attribute")
        }
        else if (!configFileJson.hasOwnProperty("accessToken")) {
            throw new Error("Config file missing accessToken attribute")
        }
        else if (!configFileJson.hasOwnProperty("shardNumber")) {
            throw new Error("Config file missing shardNumber attribute")
        }
        else if (!configFileJson.hasOwnProperty("port")) {
            throw new Error("Config file missing port attribute")
        }
        else if (!configFileJson.hasOwnProperty("coordinatorAddress")) {
            throw new Error("Config file missing coordinatorAddress attribute")
        }
        else if (!configFileJson.hasOwnProperty("coordinatorPort")) {
            throw new Error("Config file missing coordinatorPort attribute")
        }
        else if (!configFileJson.hasOwnProperty("address")) {
            throw new Error("Config file missing address attribute")
        }

        RuntimeConfigs.fsRoot = configFileJson.fsRoot
        RuntimeConfigs.shardNumber = configFileJson.shardNumber
        RuntimeConfigs.accessToken = configFileJson.accessToken
        RuntimeConfigs.port = configFileJson.port
        RuntimeConfigs.address = configFileJson.address
        RuntimeConfigs.coordinatorAddress = configFileJson.coordinatorAddress
        RuntimeConfigs.coordinatorPort = configFileJson.coordinatorPort
    }
}