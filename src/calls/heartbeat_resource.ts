import { FileShardHeartbeat } from "cheapspeech-shared";
import { Request, Response } from "express"
import RehashShard from "../background_work/rehash_shard";

export default class HeartbeatResource {

    static async get(req: Request, res: Response, next: Function) {
        var beat = new FileShardHeartbeat()
        beat.activeHashSchemas = RehashShard.activeShardHashSchemas
        res.status(200).send(JSON.stringify(beat))
    }
}