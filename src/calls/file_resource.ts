import { Request, Response } from "express";
import FsAvlStorage from "../persistence/fsavl_storage";
import FsAvlTracking from "../utility/fsavl_tracking";

export default class FileResource {

    static async postFile(req: Request, res: Response, next: Function) {
        FileResource.wrapCall(() => {
            FileResource.validateCall(req.query)

            if (FsAvlTracking.find(req.query.fileName as string)) {
                throw new FileResourceError(JSON.stringify({
                    msg: "File already exists in storage.",
                    fileExists: true
                }), 409)
            }
            if (!req.busboy) {
                throw new FileResourceError(JSON.stringify({
                    msg: "Did you forget to send the file data in the upload request?"
                }), 422)
            }

            req.busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
                FsAvlStorage.saveFile(req.query.fileName as string, file)
            })
            req.busboy.on("finish", () => {
                res.status(200).type("application/json").send(JSON.stringify({
                    msg: "File sucessfully uploaded."
                }))
            })
            req.pipe(req.busboy)
        }, res, next)
    }

    static async getFile(req: Request, res: Response, next: Function) {
        FileResource.wrapCall(() => {
            FileResource.validateCall(req.query)
            console.log("transfer started")
            let filePath = FsAvlTracking.find(req.query.fileName as string);
            if (!filePath) {
                throw new FileResourceError(JSON.stringify({
                    msg: "File not found."
                }), 404)
            }
            res.sendFile(filePath, () => { console.log("transfer complete") });
        }, res, next)
    }

    static async getFilePortion(req: Request, res: Response, next: Function) {
        FileResource.wrapCall(async () => {
            var obj = new FilePortionRetrieval(req, res)
            await obj.getPortion()
        }, res, next)
    }

    static async deleteFile(req: Request, res: Response, next: Function) {
        FileResource.wrapCall(async () => {
            FileResource.validateCall(req.query)
            var result: boolean = await FsAvlStorage.deleteFile(req.query.fileName as string)
            if (result) {
                res.sendStatus(200)
            }
            else {
                res.status(500).type("application/json").send(
                    JSON.stringify({
                        msg: "Failed to delete file."
                    })
                )
            }
        }, res, next)
    }

    static async fileExists(req: Request, res: Response, next: Function) {
        FileResource.wrapCall(async () => {
            FileResource.validateCall(req.query)
            let fileExists = FsAvlTracking.find(req.query.fileName as string) ? true : false;
            res.status(200).type("application/json").send(JSON.stringify({
                fileExists: fileExists
            }));
        }, res, next)
    }

    static validateCall(params: Object) {
        if (!params.hasOwnProperty("fileName")) {
            throw new FileResourceError(JSON.stringify({
                msg: "Invalid request, missing fileName property or parameter."
            }), 422)
        }
    }

    static async wrapCall(callback: Function, res: Response, next: Function) {
        try {
            await callback()
        }
        catch (err) {
            console.log(err)
            FileResource.errorResponse(res, err, next)
        }
    }

    static errorResponse(res: Response, err: any, next: Function) {
        if (err instanceof FileResourceError) {
            res.status(err.code).type("application/json").send(err.msg)
        }
        else {
            next(err)
        }
    }

}

class FilePortionRetrieval {
    req: Request;
    res: Response;

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    async getPortion() {

    }

    validateCall() {

    }
}

export class FileResourceError extends Error {
    msg: string = ""
    code: number = 500

    constructor(msg: string, code: number) {
        super(msg)
        this.msg = msg
        this.code = code
    }
}

