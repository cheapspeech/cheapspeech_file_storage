import express from "express";
import FileResource from "./calls/file_resource";
import PreProcess from "./utility/preprocess";
import connectBusBoy from "connect-busboy";
import fs from "fs";
import HeartbeatResource from "./calls/heartbeat_resource";
import RuntimeConfigs from "./persistence/runtime_configs";

export default class AppGlobal {
  static app = express();

  static async initialize() {
    await PreProcess.setup()

    AppGlobal.app.use(express.json())
    AppGlobal.app.use(connectBusBoy())

    AppGlobal.createRoutes()
    AppGlobal.app.use((error: Error, req: any, res: any, next: any) => {
      fs.appendFile("log.txt", error.name, () => { });
      fs.appendFile("log.txt", error.message, () => { });
      fs.appendFile("log.txt", error.stack + "\n\n" as string, () => { });
      res.status(500).type("application/json").send(JSON.stringify({
        msg: "An unidentified error occured."
      }))
    })

    AppGlobal.app.listen(RuntimeConfigs.port);
  }

  static createRoutes() {
    AppGlobal.app.post("/file", FileResource.postFile)
    AppGlobal.app.get("/file/exists", FileResource.fileExists)
    AppGlobal.app.get("/file", FileResource.getFile)
    AppGlobal.app.get("/file/portion", FileResource.getFilePortion)
    AppGlobal.app.delete("/file", FileResource.deleteFile)
    AppGlobal.app.post("/heartbeat", HeartbeatResource.get)
  }
}

AppGlobal.initialize()
