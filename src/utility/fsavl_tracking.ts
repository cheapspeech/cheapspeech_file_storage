import { AvlTree } from "datastructures-js";

export default class FsAvlTracking {
    static avl: AvlTree<string, string> = new AvlTree()

    static insert(search: string, data: string) {
        this.avl.insert(search, data)
    }

    static find(search: string): string {
        let node = this.avl.find(search)
        if (node) {
            return node.getValue()
        }
        else {
            return ""
        }
    }

    static delete(search: string) {
        this.avl.remove(search)
    }

    static replace(search: string, data: string) {
        this.avl.remove(search)
        this.avl.insert(search, data)
    }
}