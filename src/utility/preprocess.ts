import fs from "fs"
import FsAvlStorage from "../persistence/fsavl_storage"
import RuntimeConfigs from "../persistence/runtime_configs"
import GetManifest from "../background_work/get_manifest"
import RehashShard from "../background_work/rehash_shard"
import AppStatus from "../persistence/app_status"

export default class PreProcess {
    static async setup() {
        await RuntimeConfigs.readConfigs()
        await PreProcess.checkFileSystemRoot()
        await FsAvlStorage.initialize()
        await AppStatus.readAppStatus()
        GetManifest.startManifestLoop()
        RehashShard.startRehashLoop()
    }

    static async checkFileSystemRoot() {
        if (!fs.existsSync(RuntimeConfigs.fsRoot)) {
            fs.mkdirSync(RuntimeConfigs.fsRoot)
        }

        var directoryFiles = fs.readdirSync(RuntimeConfigs.fsRoot)
        if (!(directoryFiles.length > 0)) {
            for (let i = 0; i < 100; i++) {
                fs.mkdirSync(RuntimeConfigs.fsRoot + `/${i}`)
                for (let j = 0; j < 100; j++) {
                    fs.mkdirSync(RuntimeConfigs.fsRoot + `/${i}/${j}`)
                }
            }
        }
    }

    static async readAppStatusFile() {

    }

    static async createAppStatusFile() {

    }
}