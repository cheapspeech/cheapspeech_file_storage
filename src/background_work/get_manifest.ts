import { Manifest } from "cheapspeech-shared"
import RuntimeConfigs from "../persistence/runtime_configs";
import axios from "axios";

const getManifestLoopInterval = 4 * 1000

export default class GetManifest {
    static manifest: Manifest | null = null;
    static needsRegistration: boolean = false;

    static async startManifestLoop() {
        var loopFunction = async () => {
            if (GetManifest.needsRegistration) {
                console.log("Attempting registration")
                await GetManifest.registerWithCoordinator()
            }
            else {
                console.log("Getting manifest")
                await this.getManifest()
            }

            setTimeout(loopFunction, getManifestLoopInterval)
        }
        setTimeout(loopFunction, getManifestLoopInterval)
    }

    static async getManifest() {
        try {
            var url = "http://" + RuntimeConfigs.coordinatorAddress + ":" + RuntimeConfigs.coordinatorPort + "/manifest/get"
            var response = await axios.post(url, "", {
                headers: {
                    "access-token": RuntimeConfigs.accessToken
                },
            })

            var newManifest = Manifest.fromJson(response.data)
            var registeredCorrectly = newManifest.fileShardAddresses.some((elem: any) => {
                return (
                    elem.address == RuntimeConfigs.address &&
                    elem.port == RuntimeConfigs.port &&
                    elem.shardNumber == RuntimeConfigs.shardNumber
                )
            })

            if (registeredCorrectly) {
                GetManifest.manifest = newManifest
            }
            else {
                GetManifest.needsRegistration = true
                GetManifest.manifest = newManifest
            }
        }
        catch (e) {
            console.log("Failed to retrieve manifest, continuing to attempt manifest retrieval, setting current app manifest to null.")
            GetManifest.manifest = null
        }
    }

    static async registerWithCoordinator() {
        var request: string = JSON.stringify({
            address: RuntimeConfigs.address,
            port: RuntimeConfigs.port,
            shardNumber: RuntimeConfigs.shardNumber,
        })

        try {
            var response = await axios.post("http://" + RuntimeConfigs.coordinatorAddress + ":" + RuntimeConfigs.coordinatorPort + "/register/fileShard",
                request,
                {
                    headers: {
                        "access-token": RuntimeConfigs.accessToken,
                        "content-type": "application/json"
                    },
                    validateStatus: (status) => {
                        return true;
                    }
                },
            )

            this.needsRegistration = false

            if (response.status == 200) {
                this.manifest = Manifest.fromJson(response.data)
            }
        }
        catch (e) {
            console.log(e)
        }

    }
}