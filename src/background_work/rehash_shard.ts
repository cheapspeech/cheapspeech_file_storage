import { Manifest, ResourceId } from "cheapspeech-shared"
import { AvlTreeNode } from "datastructures-js"
import AppStatus from "../persistence/app_status"
import FsAvlStorage from "../persistence/fsavl_storage"
import RuntimeConfigs from "../persistence/runtime_configs"
import FsAvlTracking from "../utility/fsavl_tracking"
import GetManifest from "./get_manifest"
import fs from "fs"
import FormData from "form-data"
import axios from "axios"

const rehashLoopTimePeriod = 5 * 1000

export default class RehashShard {
    static activeShardHashSchemas: Array<number> = []
    static currentlyRehashing: boolean = false

    static startRehashLoop() {
        var loop = async () => {
            if (GetManifest?.manifest == null) {
                // If the manifest is null we don't know what the current target rehash is and so
                // we cannot start any rehash logic.  Additionally we cannot rehash to other shards
                // if we do not know where to find them.
                setTimeout(loop, rehashLoopTimePeriod)
            }
            else {
                this.checkForTargetHashInHashSchemas(GetManifest.manifest.targetHashSchema)
                if (this.shouldRehash()) {
                    // If for some reason the manifest target hash schema is null we should
                    // skip the attempted rehash for now.  DO NOT await rehashSelf, this is a
                    // fire and forget function.
                    this.rehashSelf(GetManifest.manifest.targetHashSchema)
                }
                await AppStatus.writeAppStatus()

                setTimeout(loop, rehashLoopTimePeriod)
            }
        }

        setTimeout(loop, rehashLoopTimePeriod)
    }

    static checkForTargetHashInHashSchemas(targetHashSchema: number) {
        if (!this.activeShardHashSchemas.includes(targetHashSchema)) {
            this.activeShardHashSchemas.push(targetHashSchema)
        }
    }

    static shouldRehash(): boolean {
        if (this.currentlyRehashing) {
            return false;
        }
        else if (this.activeShardHashSchemas.length > 1) {
            return true;
        }
        else {
            return false;
        }
    }

    static async rehashSelf(currentTargetHashSchema: number) {
        this.currentlyRehashing = true
        var serviceWorker = new RehashWorker(currentTargetHashSchema)

        // Once this service worker finishes rehashing the shard, new rehash periods
        // should be cleared for execution.  Once a rehash period is finished the only
        // possible hash schema for the shard should be the target that it was rehashed to.
        await serviceWorker.execute()

        RehashShard.currentlyRehashing = false
        RehashShard.activeShardHashSchemas = [currentTargetHashSchema]
        console.log("Rehash period complete")
    }
}

class RehashWorker {
    targetHashSchema: number;
    resourceId: ResourceId = new ResourceId()

    constructor(targetHashSchema: number) {
        this.targetHashSchema = targetHashSchema
    }

    async execute() {
        var filesToDelete: Array<string> = await this.traverseInOrder()
        for (var i = 0; i < filesToDelete.length; i++) {
            await FsAvlStorage.deleteFile(filesToDelete[i])
        }
    }

    async traverseInOrder(): Promise<Array<string>> {
        var filesToDelete: Array<string> = []
        var root = FsAvlTracking.avl.root()

        const cb = async (node: AvlTreeNode<string, string>) => {
            var name = node.getKey()
            var path = node.getValue()
            var hash = name.split(".")[0]
            var destinationShard = this.resourceId.getShardNumber(hash, this.targetHashSchema)
            if (destinationShard == RuntimeConfigs.shardNumber) {
                // If the destination is the current shard no movement is necessary.
                return;
            }
            else {
                var successfullyMoved = await this.moveFile(path, name, destinationShard)
                if (successfullyMoved) {
                    filesToDelete.push(name)
                }
            }
        }

        const traverseRecursive = async (current: any) => {
            if (current === null) return;
            await traverseRecursive(current.getLeft());
            await cb(current)
            await traverseRecursive(current.getRight());
        };

        await traverseRecursive(root);

        return filesToDelete
    }

    async moveFile(path: string, name: string, destinationShardNumber: number): Promise<boolean> {
        var manifest: Manifest | null = GetManifest.manifest
        // Manifest will not likely be null but it is a possibility
        if (!manifest) {
            return false;
        }

        var destination: string = "";
        for (var i = 0; i < manifest.fileShardAddresses.length; i++) {
            var shard = manifest.fileShardAddresses[i]
            if (shard.shardNumber == destinationShardNumber) {
                destination = shard.address + ":" + shard.port
                break;
            }
        }
        // If destination shard address is not present file cannot be uploaded
        if (!destination) {
            return false;
        }

        var file = fs.createReadStream(path)
        var formData = new FormData()
        formData.append("data", file)
        var url = new URL("http://" + destination + "/file")
        url.search = new URLSearchParams({ fileName: name }).toString()
        var response = await axios.post(url.toString(), formData, {
            headers: formData.getHeaders(),
            validateStatus: (status) => {
                return true;
            }
        })

        if (response.status == 200) {
            return true;
        }
        else if (response.data.hasOwnProperty("fileExists") && response.data.fileExists == true) {
            // If the file already exists it must have previously been uploaded at some point.
            return true;
        }
        else {
            return false;
        }
    }
}